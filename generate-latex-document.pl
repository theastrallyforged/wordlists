#!/usr/bin/env perl
use strict;
use warnings;

# generate-latex-document.pl -- take a dice-roll-annotated wordlist and make a pretty latex document
#
# Usage: paste-dice-rolls.pl FILE | generate-latex-document.pl >OUT.latex
#        paste-dice-rolls.pl FILE | generate-latex-document.pl | lpr

print(<<'EOF');
\documentclass[10pt]{article}

\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage{multicol}

\setlength{\parskip}{0pt}
\setlength{\parindent}{0pt}

\obeylines
\frenchspacing

\begin{document}

\begin{multicols}{4}

EOF

print($_ =~ s/\t/ \\quad /gr) while <>;

print(<<'EOF');

\end{multicols}

\end{document}
EOF
